﻿using CleanArchitecture.Core.Entities;
using CleanArchitecture.Infrastructure.Data;
using CleanArchitecture.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArchitecture.Infrastructure.Repository
{
    public class SelfMonitoringService2 : EfRepository
    {
        public SelfMonitoringService2(AppDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<SelfMonitoring> GetAll()
        {
            return this.GetAll<SelfMonitoring>();
        }


        public SelfMonitoring GetSelfMonitoring()
        {
            return this.GetSingle<SelfMonitoring>(Guid.Empty);
        }
    }
}
