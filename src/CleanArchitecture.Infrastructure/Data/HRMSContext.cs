﻿using CleanArchitecture.Core.Entities.HRMS;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArchitecture.Infrastructure.Data
{
    public class HRMSContext : DbContext
    {
        public HRMSContext(DbContextOptions<HRMSContext> options) : base(options)
        {

        }
        public DbSet<H_H215> H_H215 { get; set; }
        public DbSet<H_H21501> H_H21501 { get; set; }
    }
}
