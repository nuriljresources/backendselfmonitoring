﻿using AutoMapper;
using CleanArchitecture.Core;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.Web.Models;

namespace CleanArchitecture.Web.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserLogin, UserModel>();
            CreateMap<RegisterViewModel, UserLogin>();
            CreateMap<LoginViewModel, UserLogin>();
        }
    }
}
