﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CleanArchitecture.Web.Controllers
{
    public class ReportController : Controller
    {
        public IActionResult Index()
        {
            return Redirect("http://jrnsharepoint:8084/ReportServer/Pages/ReportViewer.aspx?%2fJResources.Report%2frptSelfMonitoring&rs:Command=Render");
        }
    }
}