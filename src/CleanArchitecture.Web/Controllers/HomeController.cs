﻿using Microsoft.AspNetCore.Mvc;
using CleanArchitecture.Web.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using CleanArchitecture.Web.CustomAttributes;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.SharedKernel.Interfaces;
using System;
using CleanArchitecture.Web.Services;
using System.Data.SqlClient;
using System.IO;
using System.Collections.Generic;
using CleanArchitecture.Infrastructure.Data;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using DinkToPdf;
using DinkToPdf.Contracts;
using System.Runtime.Loader;
using System.Reflection;
using Rotativa;
using Rotativa.AspNetCore;
using System.Net;

namespace CleanArchitecture.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository _repository;
        private readonly AppDbContext _dbContext;
        private IConverter _converter;

        public HomeController(AppDbContext dbContext, IRepository _Repository, IConverter converter)
        {
            _dbContext = dbContext;
            _repository = _Repository;
            _converter = converter;
        }

        public IActionResult Index()
        {
            SelfMonitoringModel model = new SelfMonitoringModel();
            var domainName = Request.Host.Value;

            if(domainName.Contains("api.jresources.com"))
            {
                return Redirect("https://play.google.com/store/apps/details?id=com.myjresources");
            }

            #region Site Location
            List<SelectListItem> SiteList = new List<SelectListItem>();
            var siteLocations = _dbContext.SiteLocation.ToList();
            if (siteLocations != null && siteLocations.Count > 0)
            {
                SiteList.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                {
                    Selected = true,
                    Value = "",
                    Text = "Select Site Location"
                });

                foreach (var item in siteLocations.OrderBy(x => x.nmSite))
                {
                    SiteList.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                    {
                        Value = item.kdSite,
                        Text = item.nmSite.ToUpper()
                    });
                }
            }
            #endregion

            #region Province
            List<SelectListItem> ProvinceList = new List<SelectListItem>();
            ProvinceList.Add(new SelectListItem()
            {
                Selected = true,
                Value = "",
                Text = "Select Site Province"
            });

            ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);
            string querySite = @"SELECT KdProv, NmProv FROM R_A015 WHERE KdProv != '00' AND stedit <> '2' ORDER BY NmProv";
            SqlDataReader reader = ConnectionCheck.ExecDataReader(querySite, "Year", string.Empty);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ProvinceList.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                    {
                        Value = reader.GetString(0).Trim(),
                        Text = reader.GetString(1).Trim()
                    });
                }
            }

            #endregion

            ViewBag.DomainName = domainName;
            ViewBag.SiteLocations = SiteList;
            ViewBag.Provinces = ProvinceList;
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public IActionResult SelfMonitoring(SelfMonitoringModel model)
        {
            var response = new ResponseModel();
            model.IsFromMobile = false;
            var siteLocation = _dbContext.SiteLocation.FirstOrDefault(x => x.kdSite == model.SiteCode);
            if (siteLocation != null)
            {
                model.SiteCode = siteLocation.kdSite;
                model.SiteName = siteLocation.nmSite.Trim();
            }
            else
            {
                model.SiteCode = string.Empty;
            }

            try
            {
                SelfMonitoringService _service = new SelfMonitoringService(_repository, model);
                response = _service.Save();
            }
            catch (Exception ex)
            {
                response.SetError("Error when saved : " + ex.Message);
            }

            return Ok(response);
        }


        [HttpGet]
        public IActionResult TodoList(string NIKSite)
        {
            SelfMonitoringModel model = new SelfMonitoringModel();
            ViewData.Model = model;


            return View();
        }









        [HttpGet]
        public IActionResult PrivacyPolicy()
        {
            return View();
        }



        public IActionResult Test()
        {
            UserModel objLoggedInUser = new UserModel();
            if (User.Identity.IsAuthenticated)
            {
                var claimsIndentity = HttpContext.User.Identity as ClaimsIdentity;
                var userClaims = claimsIndentity.Claims;

                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    foreach (var claim in userClaims)
                    {
                        var cType = claim.Type;
                        var cValue = claim.Value;
                        switch (cType)
                        {
                            case "UserId":
                                objLoggedInUser.Id = cValue;
                                break;
                            case "Name":
                                objLoggedInUser.FullName = cValue;
                                break;
                            case "Email":
                                objLoggedInUser.Email = cValue;
                                break;
                            case "Role":
                                objLoggedInUser.Role = cValue;
                                break;
                        }
                    }
                }
            }

            return View("Test", objLoggedInUser);
        }

        public IActionResult Employee()
        {
            return View();
        }

        public IActionResult Attendance()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Logoff()
        {
            HttpContext.Session.Clear();
            return Redirect("~/Home/Index");
        }

        public JsonResult EndSession()
        {
            HttpContext.Session.Clear();
            return Json(new { result = "success" });
        }
        private string GetRole()
        {
            if (this.HavePermission(Roles.DIRECTOR))
                return " - DIRECTOR";
            if (this.HavePermission(Roles.SUPERVISOR))
                return " - SUPERVISOR";
            if (this.HavePermission(Roles.ANALYST))
                return " - ANALYST";
            return null;
        }

        public class CustomAssemblyLoadContext : AssemblyLoadContext
        {
            public IntPtr LoadUnmanagedLibrary(string absolutePath)
            {
                return LoadUnmanagedDll(absolutePath);
            }
            protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
            {
                return LoadUnmanagedDllFromPath(unmanagedDllName);
            }
            protected override Assembly Load(AssemblyName assemblyName)
            {
                throw new NotImplementedException();
            }
        }

        [HttpGet]
        public IActionResult DocumentView()
        {

            return View();
        }


        [HttpGet]
        public IActionResult Download()
        {
            //string ddllibwkhtmltox = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "files", "libwkhtmltox.dll");
            //new CustomAssemblyLoadContext().LoadUnmanagedLibrary(ddllibwkhtmltox);

            //var globalSettings = new GlobalSettings
            //{
            //    ColorMode = ColorMode.Color,
            //    Orientation = Orientation.Portrait,
            //    PaperSize = PaperKind.A4,
            //    Margins = new MarginSettings { Top = 10 },
            //    DocumentTitle = "PDF Report",
            //    Out = Path.Combine(
            //        Directory.GetCurrentDirectory(), "wwwroot", "files", "test.pdf"
            //    )
            //};

            //var objectSettings = new ObjectSettings
            //{
            //    PagesCount = true,
            //    Page = "http://localhost:3835/Home/DocumentView",
            //    //HtmlContent = "<b>TEST TEST TEST<b>",
            //    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
            //    HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
            //    FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" }
            //};

            //var pdf = new HtmlToPdfDocument()
            //{
            //    GlobalSettings = globalSettings,
            //    Objects = { objectSettings }
            //};

            //_converter.Convert(pdf);
            //return Ok("Successfully created PDF document.");
            return new ViewAsPdf("DocumentView");
        }

        public IActionResult DownloadSKPM(String NIKSite)
        {
            return Redirect("/Reporting?NIKSite=" + NIKSite);
        }

        public IActionResult DownloadSKPMFromServer(String NIKSite)
        {
            //return Redirect("/Reporting?NIKSite=" + NIKSite);
            string uriDownload = AppSettingHelper.ReportingURL + NIKSite;
            var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot", "files", NIKSite+".pdf"
                        );

            WebClient webClient = new WebClient();
            webClient.UseDefaultCredentials = true;
            webClient.Credentials = CredentialCache.DefaultCredentials;
            webClient.DownloadFile(uriDownload, path);


            return new PhysicalFileResult(path, "application/pdf");
        }
    }
}
