﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CleanArchitecture.Web.Controllers
{
    public class DownloadController : Controller
    {
        public IActionResult Index()
        {
            return Redirect("https://play.google.com/store/apps/details?id=com.myjresources");
        }
    }
}