using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.Infrastructure.Data;
using CleanArchitecture.Infrastructure.Repository;
using CleanArchitecture.SharedKernel.Interfaces;
using CleanArchitecture.Web.CustomAttributes;
using CleanArchitecture.Web.Models;
using CleanArchitecture.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Rotativa.AspNetCore;

namespace CleanArchitecture.Web.Controllers
{
    public class SelfMonitoringController : Controller
    {
        private readonly IRepository _repository;
        private readonly AppDbContext _dbContext;
        public SelfMonitoringController(AppDbContext dbContext, IRepository Repository)
        {
            _dbContext = dbContext;
            _repository = Repository;
        }

        public IActionResult History(string nikSite, string dateStart)
        {
            var items = new List<SelfMonitoringViewModel>();

            if (!string.IsNullOrEmpty(nikSite))
            {
                nikSite = nikSite.ToUpper();
                DateTime dtStart = DateTime.Now.Date;
                if (!string.IsNullOrEmpty(dateStart))
                {
                    var isValid = DateTime.TryParse(dateStart, out dtStart);
                    if (!isValid)
                    {
                        dtStart = DateTime.Now.Date;
                    }
                }
                var _service = new SelfMonitoringService(_repository, new SelfMonitoringModel());
                items = _service.Get14DaysHistory(nikSite, dtStart);
            }

            


            var _listSuhuTubuh = new StringBuilder();
            _listSuhuTubuh.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listSuhuTubuh.Append("{");
                _listSuhuTubuh.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    ,(item.DateTimeCheckIn.Month - 1).ToString("##")
                    ,(item.DateTimeCheckIn.Day).ToString("##")
                    ,(item.BodyTemperatureNight > 0 ? item.BodyTemperatureNight : item.BodyTemperatureMorning)));
                _listSuhuTubuh.Append("},");
                _listSuhuTubuh.AppendLine("");
            }
            _listSuhuTubuh.AppendLine("]");


            var _listNoData = new StringBuilder();
            _listNoData.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listNoData.Append("{");
                _listNoData.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    , (item.DateTimeCheckIn.Month - 1).ToString("##")
                    , (item.DateTimeCheckIn.Day).ToString("##")
                    , (item.IsCheckIn ? 0 : 10)));
                _listNoData.Append("},");
                _listNoData.AppendLine("");
            }
            _listNoData.AppendLine("]");




            var _listSoreThroat = new StringBuilder();
            _listSoreThroat.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listSoreThroat.Append("{");
                _listSoreThroat.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    , (item.DateTimeCheckIn.Month - 1).ToString("##")
                    , (item.DateTimeCheckIn.Day).ToString("##")
                    , (item.IsSoreThroat ? 10 : 0)));
                _listSoreThroat.Append("},");
                _listSoreThroat.AppendLine("");
            }
            _listSoreThroat.AppendLine("]");



            //IS FLU
            var _listFlu = new StringBuilder();
            _listFlu.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listFlu.Append("{");
                _listFlu.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    , (item.DateTimeCheckIn.Month - 1).ToString("##")
                    , (item.DateTimeCheckIn.Day).ToString("##")
                    , (item.IsFlu ? 10 : 0)));
                _listFlu.Append("},");
                _listFlu.AppendLine("");
            }
            _listFlu.AppendLine("]");


            // Is Fever
            var _listFever = new StringBuilder();
            _listFever.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listFever.Append("{");
                _listFever.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    , (item.DateTimeCheckIn.Month - 1).ToString("##")
                    , (item.DateTimeCheckIn.Day).ToString("##")
                    , (item.IsFever ? 10 : 0)));
                _listFever.Append("},");
                _listFever.AppendLine("");
            }
            _listFever.AppendLine("]");


            // Cough
            var _listCough = new StringBuilder();
            _listCough.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listCough.Append("{");
                _listCough.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    , (item.DateTimeCheckIn.Month - 1).ToString("##")
                    , (item.DateTimeCheckIn.Day).ToString("##")
                    , (item.IsCough ? 10 : 0)));
                _listCough.Append("},");
                _listCough.AppendLine("");
            }
            _listCough.AppendLine("]");


            // Shortness
            var _listShortness = new StringBuilder();
            _listShortness.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listShortness.Append("{");
                _listShortness.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    , (item.DateTimeCheckIn.Month - 1).ToString("##")
                    , (item.DateTimeCheckIn.Day).ToString("##")
                    , (item.IsShortness ? 10 : 0)));
                _listShortness.Append("},");
                _listShortness.AppendLine("");
            }
            _listShortness.AppendLine("]");

            // ContactPatient
            var _listContactPatient = new StringBuilder();
            _listContactPatient.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listContactPatient.Append("{");
                _listContactPatient.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    , (item.DateTimeCheckIn.Month - 1).ToString("##")
                    , (item.DateTimeCheckIn.Day).ToString("##")
                    , (item.IsContactPatient ? 10 : 0)));
                _listContactPatient.Append("},");
                _listContactPatient.AppendLine("");
            }
            _listContactPatient.AppendLine("]");

            // Quarantine
            var _listQuarantine = new StringBuilder();
            _listQuarantine.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _listQuarantine.Append("{");
                _listQuarantine.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                    , item.DateTimeCheckIn.Year
                    , (item.DateTimeCheckIn.Month - 1).ToString("##")
                    , (item.DateTimeCheckIn.Day).ToString("##")
                    , (item.IsQuarantine ? 10 : 0)));
                _listQuarantine.Append("},");
                _listQuarantine.AppendLine("");
            }
            _listQuarantine.AppendLine("]");




            Dictionary<string, string> dicChartData = new Dictionary<string, string>();
            dicChartData.Add("SUHU_TUBUH", _listSuhuTubuh.ToString());
            dicChartData.Add("NO_DATA", _listNoData.ToString());
            dicChartData.Add("SORETHROAT", _listSoreThroat.ToString());
            dicChartData.Add("FLU", _listFlu.ToString());
            dicChartData.Add("FEVER", _listFever.ToString());
            dicChartData.Add("COUGH", _listCough.ToString());
            dicChartData.Add("SHORTNESS", _listShortness.ToString());
            dicChartData.Add("CONTACT_PATIENT", _listContactPatient.ToString());
            dicChartData.Add("QUARANTINE", _listQuarantine.ToString());




            ViewBag.NIKSite = nikSite;
            ViewBag.DateStart = dateStart;
            ViewBag.DataChart = dicChartData;
            return View(items);
        }

        public IActionResult HistoryJSON()
        {
            string nikSite = "JRN0291";
            string dateStart = "";

            var items = new List<SelfMonitoringViewModel>();

            if (!string.IsNullOrEmpty(nikSite))
            {
                nikSite = nikSite.ToUpper();
                DateTime dtStart = DateTime.Now.Date;
                if (!string.IsNullOrEmpty(dateStart))
                {
                    var isValid = DateTime.TryParse(dateStart, out dtStart);
                    if (!isValid)
                    {
                        dtStart = DateTime.Now.Date;
                    }
                }
                var _service = new SelfMonitoringService(_repository, new SelfMonitoringModel());
                items = _service.Get14DaysHistory(nikSite, dtStart);
            }

            ViewBag.NIKSite = nikSite;
            ViewBag.DateStart = dateStart;

            var _list = new StringBuilder();
            _list.AppendLine("[");
            foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
            {
                _list.AppendLine(string.Format("[{0}, {1}],", item.DateTimeCheckIn.Day, (item.BodyTemperatureNight > 0 ? item.BodyTemperatureNight : item.BodyTemperatureMorning)));
            }
            _list.AppendLine("]");

            return Ok(_list.ToString());
        }


        [HttpGet]
        public IActionResult Download()
        {
            var items = new List<SelfMonitoringViewModel>();

            var _service = new SelfMonitoringService(_repository, new SelfMonitoringModel());
            items = _service.Get14DaysHistory("JRN0291", DateTime.Now);

            ViewBag.NIKSite = "JRN0291";
            ViewBag.DateStart = "";

            return new ViewAsPdf("History", items);
        }


        public IActionResult Detail()
        {
            SelfMonitoringModel model = new SelfMonitoringModel();
            var domainName = Request.Host.Value;

            if (domainName.Contains("api.jresources.com"))
            {
                return Redirect("https://play.google.com/store/apps/details?id=com.myjresources");
            }

            #region Site Location
            List<SelectListItem> SiteList = new List<SelectListItem>();
            var siteLocations = _dbContext.SiteLocation.ToList();
            if (siteLocations != null && siteLocations.Count > 0)
            {
                SiteList.Add(new SelectListItem()
                {
                    Selected = true,
                    Value = "",
                    Text = "Select Site Location"
                });

                foreach (var item in siteLocations.OrderBy(x => x.nmSite))
                {
                    SiteList.Add(new SelectListItem()
                    {
                        Value = item.kdSite,
                        Text = item.nmSite.ToUpper()
                    });
                }
            }
            #endregion

            #region Province
            List<SelectListItem> ProvinceList = new List<SelectListItem>();
            ProvinceList.Add(new SelectListItem()
            {
                Selected = true,
                Value = "",
                Text = "Select Site Province"
            });


            ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);
            string querySite = @"SELECT KdProv, NmProv FROM R_A015 WHERE KdProv != '00' AND stedit <> '2' ORDER BY NmProv";
            SqlDataReader reader = ConnectionCheck.ExecDataReader(querySite, "Year", string.Empty);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ProvinceList.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                    {
                        Value = reader.GetString(0).Trim(),
                        Text = reader.GetString(1).Trim()
                    });
                }
            }

            #endregion

            ViewBag.DomainName = domainName;
            ViewBag.SiteLocations = SiteList;
            ViewBag.Provinces = ProvinceList;
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public IActionResult Save(SelfMonitoringModel model)
        {
            var response = new ResponseModel();
            model.NIKSite = DataEncription.Decrypt(model.Token);
            model.IsFromMobile = true;

            var siteLocation = _dbContext.SiteLocation.FirstOrDefault(x => x.kdSite == model.SiteCode);
            if (siteLocation != null)
            {
                model.SiteCode = siteLocation.kdSite;
                model.SiteName = siteLocation.nmSite.Trim();
            }
            else
            {
                model.SiteCode = string.Empty;
            }

            try
            {
                SelfMonitoringService _service = new SelfMonitoringService(_repository, model);
                response = _service.Save();
            }
            catch (Exception ex)
            {
                response.SetError("Error when saved : " + ex.Message);
            }

            return Ok(response);
        }



        [HttpPost]
        public IActionResult GetList(PagingParameterModel param)
        {
            int PageSize = AppSettingHelper.PageSize;

            var source = _repository.GetAll<SelfMonitoring>()
                .Where(x => x.NIKSite == param.NIKSite)
                .OrderByDescending(x => x.DateCheckIn);

            param.TotalCount = source.Count();
            param.TotalPages = (int)Math.Ceiling(param.TotalCount / (double)PageSize);
            var items = source.Skip((param.PageNumber - 1) * PageSize).Take(PageSize)
                .Select(x => new SelfMonitoringViewModel()
                {
                    DateTimeCheckIn = x.DateCheckIn,
                    NIKSite = x.NIKSite,
                    FullName = x.FullName,
                    Photo = (!string.IsNullOrEmpty(x.PhotoNight) ? x.PhotoNight : x.PhotoMorning),
                    SiteName = x.SiteName,
                    CompanyName = x.CompanyName,
                    BodyTemperatureMorning = (x.BodyTemperature.HasValue ? x.BodyTemperature.Value : 0),
                    BodyTemperatureNight = (x.BodyTemperatureNight.HasValue ? x.BodyTemperatureNight.Value : 0),
                    IsSoreThroat = x.IsSoreThroat,
                    IsFlu = x.IsFlu,
                    IsFever = x.IsFever,
                    IsCough = x.IsCough,
                    IsShortness = x.IsShortness,
                    IsOnDuty = x.IsOnDuty,
                    IsContactPatient = x.IsContactPatient,
                    IsQuarantine = x.IsQuarantine,
                }).ToList();

            param.IsNext = (param.TotalPages > param.PageNumber);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(param));
            return Ok(items);
        }
    }
}
