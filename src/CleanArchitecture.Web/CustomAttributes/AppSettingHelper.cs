﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArchitecture.Web.CustomAttributes
{
    public class AppSettingHelper
    {
        public static IConfigurationRoot AppSetting
        {
            get
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();

                return configuration;
            }
        }

        public static string HRMSConnectionString
        {
            get
            {
                return AppSetting.GetConnectionString("HRMSConnection");
            }
        }

        public static int PageSize
        {
            get
            {
                return AppSetting.GetValue<int>("PageSize");
            }
        }

        public static string JWTSecretKey
        {
            get
            {
                return AppSetting.GetValue<string>("JWTSecretKey");
            }
        }

        public static bool IsDevelopment
        {
            get
            {
                return AppSetting.GetValue<bool>("IsDevelopment");
            }
        }

        public static string ReportingURL
        {
            get { return AppSetting.GetValue<string>("Reporting:url"); }
        }

        public static string ReportingNik
        {
            get { return AppSetting.GetValue<string>("Reporting:nik"); }
        }

        public static string ReportingPassword
        {
            get { return AppSetting.GetValue<string>("Reporting:password"); }
        }

        public static string MailServer
        {
            get { return AppSetting.GetValue<string>("Mail:server"); }
        }

        public static string MailPort
        {
            get { return AppSetting.GetValue<string>("Mail:port"); }
        }

        public static string MailUser
        {
            get { return AppSetting.GetValue<string>("Mail:user"); }
        }

        public static string MailPassword
        {
            get { return AppSetting.GetValue<string>("Mail:password"); }
        }

        public static string MailFrom
        {
            get { return AppSetting.GetValue<string>("Mail:from"); }
        }

        public static string MailTo
        {
            get { return AppSetting.GetValue<string>("Mail:to"); }
        }

        public static string MailBcc
        {
            get { return AppSetting.GetValue<string>("Mail:bcc"); }
        }
    }
}
