using System;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.Infrastructure.Data;
using CleanArchitecture.Infrastructure.Repository;
using CleanArchitecture.SharedKernel.Interfaces;
using CleanArchitecture.Web.CustomAttributes;
using CleanArchitecture.Web.Models;
using CleanArchitecture.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CleanArchitecture.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SelfMonitoringController : Controller
    {
        private readonly IRepository _repository;
        private readonly AppDbContext _dbContext;
        public SelfMonitoringController(AppDbContext dbContext, IRepository Repository)
        {
            _dbContext = dbContext;
            _repository = Repository;
        }

        [HttpPost("Save")]
        public IActionResult Save(SelfMonitoringModel model)
        {
            var response = new ResponseModel();
            model.NIKSite = DataEncription.Decrypt(model.Token);
            model.IsFromMobile = true;

            var siteLocation = _dbContext.SiteLocation.FirstOrDefault(x => x.kdSite == model.SiteCode);
            if (siteLocation != null)
            {
                model.SiteCode = siteLocation.kdSite;
                model.SiteName = siteLocation.nmSite.Trim();
            }
            else
            {
                model.SiteCode = string.Empty;
            }

            try
            {
                SelfMonitoringService _service = new SelfMonitoringService(_repository, model);
                response = _service.Save();
            }
            catch (Exception ex)
            {
                response.SetError("Error when saved : " + ex.Message);
            }

            return Ok(response);
        }


        [HttpPost("List")]
        public IActionResult List(param14Quarantine param)
        {
            //int PageSize = AppSettingHelper.PageSize;

            //var source = _repository.GetAll<SelfMonitoring>()
            //    .Where(x => x.NIKSite == param.NIKSite)
            //    .OrderByDescending(x => x.DateCheckIn);

            //param.TotalCount = source.Count();
            //param.TotalPages = (int)Math.Ceiling(param.TotalCount / (double)PageSize);
            //var items = source.Skip((param.PageNumber - 1) * PageSize).Take(PageSize)
            //    .Select(x => new SelfMonitoringViewModel()
            //    {
            //        DateTimeCheckIn = x.DateCheckIn,
            //        NIKSite = x.NIKSite,
            //        FullName = x.FullName,
            //        Photo = (!string.IsNullOrEmpty(x.PhotoNight) ? x.PhotoNight : x.PhotoMorning),
            //        SiteName = x.SiteName,
            //        CompanyName = x.CompanyName,
            //        BodyTemperatureMorning = (x.BodyTemperature.HasValue ? x.BodyTemperature.Value : 0),
            //        BodyTemperatureNight = (x.BodyTemperatureNight.HasValue ? x.BodyTemperatureNight.Value : 0),
            //        IsSoreThroat = x.IsSoreThroat,
            //        IsFlu = x.IsFlu,
            //        IsFever = x.IsFever,
            //        IsCough = x.IsCough,
            //        IsShortness = x.IsShortness,
            //        IsOnDuty = x.IsOnDuty,
            //        IsContactPatient = x.IsContactPatient,
            //        IsQuarantine = x.IsQuarantine,
            //    }).ToList();

            //param.IsNext = (param.TotalPages > param.PageNumber);
            

            SelfMonitoringService _service = new SelfMonitoringService(_repository, new SelfMonitoringModel());

            DateTime dateStart = DateTime.Now.Date;
            if(!string.IsNullOrEmpty(param.DateTimeStart))
            {
                var isValid = DateTime.TryParse(param.DateTimeStart, out dateStart);
                if (!isValid)
                {
                    dateStart = DateTime.Now.Date;
                }
            }

            var items = _service.Get14DaysHistory(param.NIKSite, dateStart);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(param));
            return Ok(items);
        }
    }
}
