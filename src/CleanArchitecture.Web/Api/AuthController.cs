﻿using System;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.Infrastructure.Data;
using CleanArchitecture.SharedKernel.Interfaces;
using CleanArchitecture.Web.CustomAttributes;
using CleanArchitecture.Web.Models;
using CleanArchitecture.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace CleanArchitecture.Web.Api
{
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : Controller
    {
        private readonly IRepository _repository;
        private readonly AppDbContext _dbContext;
        public AuthController(AppDbContext dbContext, IRepository Repository)
        {
            _dbContext = dbContext;
            _repository = Repository;
        }

        //POST: api/Login
        [HttpPost("Login")]
        public IActionResult Login([FromBody]LoginViewModel model)
        {
            var response = new ResponseModel();
            UserMobileModel userModel = new UserMobileModel();
            try
            {
                if (model.IsHRMS)
                {
                    ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);
                    string NIKSite = model.Username.Trim();
                    string queryEMP = string.Format(
                        @"SELECT NIKSITE, Nama, NmCompany, nmDepar, nmJabat, kdSite FROM V_HRMS_EMP WHERE NIKSite = '{0}' AND active = 1
                        UNION ALL
                        SELECT NIK, Nama,
                        ISNULL((select nmVenLS from V_HRMS_LSPOS where V_HRMS_LSEMP.vendor = V_HRMS_LSPOS.vendor), '') AS NmCompany,
                        '' AS nmDepar,
                        ISNULL((select nmjabat from V_HRMS_LSVENDOR where Kdjabat = V_HRMS_LSEMP.kdjabatan), '') AS nmJabat, 
                        kdSite  
                        FROM V_HRMS_LSEMP WHERE NIK = '{0}' AND active = 1"
                    , NIKSite);
                    SqlDataReader reader = ConnectionCheck.ExecDataReader(queryEMP
                        , "Year", model.Username.Trim());

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            userModel.NIKSite = reader.GetString(0);
                            userModel.FullName = reader.GetString(1);
                            userModel.CompanyName = reader.GetString(2);
                            userModel.DepartmentName = reader.GetString(3);
                            userModel.Role = reader.GetString(4);
                            userModel.SiteCode = reader.GetString(5);

                            userModel.Token = DataEncription.Encrypt(userModel.NIKSite);
                        }
                        userModel.IsHRMS = true;
                        response.SetSuccess();
                        response.ResponseObject = userModel;
                    } else
                    {
                        response.SetError("NIKSite " + model.Username.Trim() + " tidak ditemukan");
                    }
                }
                else
                {
                    userModel.NIKSite = model.Username;
                    userModel.FullName = model.FullName;
                    userModel.CompanyName = model.CompanyName;
                    userModel.DepartmentName = string.Empty;
                    userModel.Role = string.Empty;
                    userModel.IsHRMS = false;
                    userModel.SiteCode = string.Empty;
                    userModel.Token = DataEncription.Encrypt(model.Username);

                    response.SetSuccess();
                    response.ResponseObject = userModel;
                }

            } catch(Exception ex)
            {
                response.SetError("Terjadi kesalahan saat diproses : " + ex.Message);
            }

            return Ok(response);
        }


        // POST: api/Register
        [HttpPost("Register")]
        public IActionResult Register([FromBody]RegisterViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var emailUniq = _repository.GetSingle<UserLogin>(x => x.Email == model.Email) != null;
            if (emailUniq) return BadRequest(new { email = "user with this email already exists" });

            var usernameUniq = _repository.GetSingle<UserLogin>(x => x.Username == model.Username) != null;
            if (usernameUniq) return BadRequest(new { username = "user with this email already exists" });

            var user = new UserLogin
            {
                Id = Guid.NewGuid(),
                Username = model.Username,
                Email = model.Email,
                FullName = model.FullName,
                Role = String.Empty,
                //Password = authService.HashPassword(model.Password)
            };
            _repository.Add(user);
            _repository.Commit();

            //return Ok(authService.GetAuthData(user));
            return Ok(true);
        }

        // POST: api/Attendance
        [HttpPost("SelfMonitoring")]
        public IActionResult SelfMonitoring(SelfMonitoringModel model)
        {
            var response = new ResponseModel();
            model.NIKSite = DataEncription.Decrypt(model.Token);
            model.IsFromMobile = true;

            var siteLocation = _dbContext.SiteLocation.FirstOrDefault(x => x.kdSite == model.SiteCode);
            if(siteLocation != null)
            {
                model.SiteCode = siteLocation.kdSite;
                model.SiteName = siteLocation.nmSite.Trim();
            } else
            {
                model.SiteCode = string.Empty;
            }

            try
            {
                SelfMonitoringService _service = new SelfMonitoringService(_repository, model);
                response = _service.Save();
            } catch(Exception ex)
            {
                response.SetError("Terjadi kesalahan saat menyimpan : " + ex.Message);
            }

            return Ok(response);
        }

        [HttpPost("Attendance")]
        public IActionResult Attendance(SelfMonitoringModel model)
        {
            var response = new ResponseModel();
            model.NIKSite = DataEncription.Decrypt(model.Token);
            model.IsFromMobile = true;

            var siteLocation = _dbContext.SiteLocation.FirstOrDefault(x => x.kdSite == model.SiteCode);
            if (siteLocation != null)
            {
                model.SiteCode = siteLocation.kdSite;
                model.SiteName = siteLocation.nmSite.Trim();
            }
            else
            {
                model.SiteCode = string.Empty;
            }

            try
            {
                SelfMonitoringService _service = new SelfMonitoringService(_repository, model);
                response = _service.Save();
            }
            catch (Exception ex)
            {
                response.SetError("Terjadi kesalahan saat menyimpan : " + ex.Message);
            }

            return Ok(response);
        }


        //[Authorize]
        [HttpGet("GetProfile")]
        public IActionResult GetCurrentUser()
        {
            //var user = _repository.GetSingle<UserLogin>(x => x.Username == model.Username);

            UserModel model = new UserModel();
            if (User.Identity.IsAuthenticated)
            {
                var claimsIndentity = HttpContext.User.Identity as ClaimsIdentity;
                var userClaims = claimsIndentity.Claims;

                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    foreach (var claim in userClaims)
                    {
                        var cType = claim.Type;
                        var cValue = claim.Value;
                        switch (cType)
                        {
                            case "UserId":
                                model.Id = cValue;
                                break;
                            case ClaimTypes.Name:
                                model.FullName = cValue;
                                break;
                            case ClaimTypes.Email:
                                model.Email = cValue;
                                break;
                            case ClaimTypes.Role:
                                model.Role = cValue;
                                break;
                        }
                    }
                }
            }

            return Ok(model);
        }


        [AllowAnonymous]
        [HttpPost("Authenticate")]
        #region OLD Login
        public IActionResult Authenticate([FromBody]LoginViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest("Exeception Active Directory");

            UserMobileModel userModel = new UserMobileModel();
            if (!string.IsNullOrEmpty(model.Username) && !string.IsNullOrEmpty(model.Password))
            {
                bool IsValidUsername = false;
                string CompanyCode = string.Empty;
                string Username = string.Empty;

                var username = model.Username.Split('\\');
                if (username != null && username.Length > 1 &&
                    !string.IsNullOrEmpty(username[0]) &&
                    !string.IsNullOrEmpty(username[1]))
                {
                    CompanyCode = username[0];
                    Username = username[1];
                    IsValidUsername = true;
                }

                if (IsValidUsername)
                {
                    username = model.Username.Split('\\');
                    DirectoryEntry _entry = new DirectoryEntry(string.Format("LDAP://{0}", username[0]), username[1], model.Password);


                    DirectorySearcher _searcher = new DirectorySearcher(_entry);
                    _searcher.Filter = string.Format("(samAccountName={0})", username[1]);

                    try
                    {
                        SearchResult _sr = _searcher.FindOne();
                        if (_sr != null)
                        {
                            userModel.NIKSite = _sr.Properties["employeeid"][0].ToString();

                            var propertyCompany = _sr.Properties["company"];
                            if (propertyCompany != null && propertyCompany.Count > 0)
                            {
                                if (propertyCompany[0] != null && !string.IsNullOrEmpty(propertyCompany[0].ToString()))
                                {
                                    userModel.CompanyName = propertyCompany[0].ToString();
                                }
                            }

                            userModel.FullName = _sr.Properties["name"][0].ToString();
                            userModel.Username = _sr.Properties["samaccountname"][0].ToString();
                            userModel.Email = _sr.Properties["mail"][0].ToString();
                            userModel.CompanyName = username[0];
                        }
                    }
                    catch (Exception ex)
                    {
                        return BadRequest("Exeception Active Directory");
                    }

                    if (!string.IsNullOrEmpty(userModel.NIKSite))
                    {
                        ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);
                        string NIKSite = userModel.NIKSite.Trim();
                        string queryEMP = string.Format("SELECT TOP 1 NIKSITE, Nama, NmCompany, nmDepar, nmJabat FROM V_HRMS_EMP WHERE NIKSite = '{0}'", NIKSite);
                        SqlDataReader reader = ConnectionCheck.ExecDataReader(queryEMP, "", userModel.NIKSite.Trim());

                        while (reader.Read())
                        {
                            userModel.NIKSite = reader.GetString(0);
                            userModel.FullName = reader.GetString(1);
                            userModel.CompanyName = reader.GetString(2);
                            userModel.DepartmentName = reader.GetString(3);
                            userModel.Role = reader.GetString(4);
                        }

                        var tokenHandler = new JwtSecurityTokenHandler();
                        var key = Encoding.ASCII.GetBytes(AppSettingHelper.JWTSecretKey);
                        var tokenDescriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(new Claim[]
                            {
                                    new Claim(ClaimTypes.Name, userModel.NIKSite),
                            }),

                            Expires = DateTime.UtcNow.AddDays(7),
                            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                        };
                        var token = tokenHandler.CreateToken(tokenDescriptor);
                        var tokenString = tokenHandler.WriteToken(token);

                    }
                    else
                    {
                        return BadRequest("Failed login Active Directory");
                    }
                }
                else
                {
                    return BadRequest("Format Username Invalid");
                }
            }
            else
            {
                return BadRequest("Username & Password cant be null");
            }


            return Ok(userModel);
        }
        #endregion


        //public IActionResult Authenticate([FromBody]LoginViewModel model)
        //{
        //    var user = _userService.Authenticate(model.Username, model.Password);

        //    if (user == null)
        //        return BadRequest(new { message = "Username or password is incorrect" });

        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new Claim[]
        //        {
        //            new Claim(ClaimTypes.Name, user.Id.ToString())
        //        }),
        //        Expires = DateTime.UtcNow.AddDays(7),
        //        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        //    };
        //    var token = tokenHandler.CreateToken(tokenDescriptor);
        //    var tokenString = tokenHandler.WriteToken(token);

        //    // return basic user info and authentication token
        //    return Ok(new
        //    {
        //        Id = user.Id,
        //        Username = user.Username,
        //        FirstName = user.FirstName,
        //        LastName = user.LastName,
        //        Token = tokenString
        //    });
        //}


    }
}