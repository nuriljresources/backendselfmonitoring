﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.Infrastructure.Data;
using CleanArchitecture.SharedKernel.Interfaces;
using CleanArchitecture.Web.CustomAttributes;
using CleanArchitecture.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace CleanArchitecture.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LookupController : ControllerBase
    {
        private readonly IRepository _repository;
        private readonly AppDbContext _dbContext;

        public LookupController(IRepository Repository, AppDbContext dbContext)
        {
            _repository = Repository;
            _dbContext = dbContext;
        }


        [HttpGet("SiteLocation")]
        [ResponseCache(Duration = 30)]
        public IActionResult SiteLocation()
        {
            List<SelectListItem> SiteList = new List<SelectListItem>();

            var siteLocations = _dbContext.SiteLocation.ToList();
            if(siteLocations != null && siteLocations.Count > 0)
            {
                SiteList.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                {
                    Value = "",
                    Text = "Select Site Location"
                });

                foreach (var item in siteLocations.OrderBy(x => x.nmSite))
                {
                    SiteList.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                    {
                        Group = new SelectListGroup() {  
                            Name = item.kdCompany 
                        },
                        Value = item.kdSite,
                        Text = item.nmSite.ToUpper()
                    });
                }
            }




            return Ok(SiteList);
        }

        [HttpGet("AreaProvince")]
        public IActionResult AreaProvince(string KdProv)
        {
            List<SelectListItem> AreaList = new List<SelectListItem>();
            AreaList.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
            {
                Selected = true,
                Value = "",
                Text = "Select Area"
            });


            ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);
            string querySite = string.Format("SELECT KdKota, NmKota FROM R_A014 WHERE KdProv = '{0}' AND StEdit <> '2' ORDER BY NmKota", KdProv);
            SqlDataReader reader = ConnectionCheck.ExecDataReader(querySite, "Year", string.Empty);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    AreaList.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                    {
                        Value = reader.GetString(0).Trim(),
                        Text = reader.GetString(1).Trim()
                    });
                }
            }

            return Ok(AreaList);
        }


        [HttpGet("GetSpecialist")]
        [ResponseCache(Duration = 30)]
        public IActionResult GetSpecialist()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem()
            {
                Selected = true,
                Value = "",
                Text = "Select Specialist"
            });


            ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);
            string querySite = string.Format("SELECT kdspesialis, nmspesialis from H_A21505 ORDER BY nmspesialis", string.Empty);
            SqlDataReader reader = ConnectionCheck.ExecDataReader(querySite, "Year", string.Empty);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    items.Add(new SelectListItem()
                    {
                        Value = reader.GetString(0).Trim(),
                        Text = reader.GetString(1).Trim()
                    });
                }
            }

            return Ok(items);
        }
    }
}