﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CleanArchitecture.Infrastructure.Data;
using CleanArchitecture.SharedKernel.Interfaces;
using CleanArchitecture.Web.CustomAttributes;
using CleanArchitecture.Web.Models;
using CleanArchitecture.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CleanArchitecture.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicalClaimController : ControllerBase
    {
        private readonly IRepository _repository;
        private readonly AppDbContext _dbContext;
        public MedicalClaimController(AppDbContext dbContext, IRepository Repository)
        {
            _dbContext = dbContext;
            _repository = Repository;
        }

        [HttpPost("OutPatient")]
        public IActionResult OutPatient(MedicalOutPatientModel model)
        {
            ResponseModel response = new ResponseModel();

            ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);
            ConnectionCheck.BeginTransaction();

            model.DocumentNo = "TEST-" + Guid.NewGuid().ToString().Split("-")[0];

            string queryHeader = string.Format(
                @"INSERT INTO H_H215 (
                    NoReg, KdSite, TglTrans, Tipe, Nik, fstatus, Keterangan, CreatedBy, CreatedIn, CreatedTime, StEdit, pycostcode
                ) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')",
                model.DocumentNo,
                model.KdSite,
                model.TglTrans,
                model.Tipe,
                model.Nik,
                model.fstatus,
                model.Keterangan,
                model.CreatedBy,
                System.Environment.MachineName,
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                model.StEdit,
                model.pycostcode);



            ConnectionCheck.ExecScalar(queryHeader, "", string.Empty);


            foreach (var item in model.Details)
            {
                string queryDetail = string.Format(
                @"INSERT INTO H_H21501 (
                    NoReg, TglDet, Person, PersenClaim, BiayaObat, BiayaKons, BiayaLab, BiayaLain, BiayaTot, 
                    tdokter, jdokter, kdspesialis, kdDiagnosa, dremarks, dokter_remark
                ) VALUES ('{0}','{1}','{2}','{3}', {4}, {5}, {6}, {7}, {8},'{9}','{10}','{11}','{12}','{13}')",
                model.DocumentNo,
                item.TglDet,
                item.Person,
                item.PersenClaim,
                item.BiayaObat,
                item.BiayaKons,
                item.BiayaLab,
                item.BiayaTot,
                item.tDokter,
                item.JDokter,
                item.kdSpesialis,
                item.kdDiagnosa,
                item.dremarks,
                item.dokter_remark);

                ConnectionCheck.ExecScalar(queryDetail, "", string.Empty);
            }

            ConnectionCheck.Commit();
            response.SetSuccess(string.Format("Document No {0} success to save", model.DocumentNo));

            return Ok(response);
        }

        [HttpPost("GetOutPatient")]
        public IActionResult GetOutPatient()
        {
            return Ok(true);
        }

    }
}