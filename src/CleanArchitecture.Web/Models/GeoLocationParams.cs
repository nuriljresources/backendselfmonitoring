﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArchitecture.Web.Models
{
    public class GeoLocationParams
    {
        public string Longitude { get; set; }
        public string Latitude { get; set; }
    }
}
