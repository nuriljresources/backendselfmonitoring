﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArchitecture.Web.Models
{
    public class SelfMonitoringModel
    {
        public SelfMonitoringModel()
        {
            IsOnDuty = true;
        }

        public string Token { get; set; }
        public IFormFile Image { get; set; }
        public string Maps { get; set; }

        public bool IsHRMS { get; set; }

        public string NIKSite { get; set; }
        public string NIKCompany { get; set; }
        public string FullName { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string CompanyName { get; set; }


        public string DateTimeCheckInStr { get; set; }
        public DateTime DateTimeCheckIn { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public decimal BodyTemperature { get; set; }
        public string BodyTemperatureStr { get; set; }
        public bool IsSoreThroat { get; set; }
        public bool IsFlu { get; set; }
        public bool IsFever { get; set; }
        public bool IsCough { get; set; }
        public bool IsShortness { get; set; }
        public bool IsOnDuty { get; set; }
        public bool IsContactPatient { get; set; }
        public bool IsQuarantine { get; set; }

        public bool IsFromMobile { get; set; }
        public string ProvinceArea { get; set; }
    }

    public class SelfMonitoringViewModel
    {
        public DateTime DateTimeCheckIn { get; set; }
        public bool IsCheckIn { get; set; } = false;
        public string NIKSite { get; set; }
        public string FullName { get; set; }
        public string Photo { get; set; }
        public string SiteName { get; set; }
        public string CompanyName { get; set; }        

        public decimal BodyTemperatureMorning { get; set; }
        public decimal BodyTemperatureNight { get; set; }

        public bool IsSoreThroat { get; set; }
        public bool IsFlu { get; set; }
        public bool IsFever { get; set; }
        public bool IsCough { get; set; }
        public bool IsShortness { get; set; }
        public bool IsOnDuty { get; set; }
        public bool IsContactPatient { get; set; }
        public bool IsQuarantine { get; set; }
    }
}
