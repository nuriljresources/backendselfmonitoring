﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArchitecture.Web.Models
{
    public class MedicalOutPatientModel
    {
        public MedicalOutPatientModel()
        {
            Details = new List<MedicalOutPatientDetailModel>();
        }
        public string DocumentNo { get; set; }
        public string NoRef { get; set; }
        public string KdSite { get; set; }
        public DateTime? TglTrans { get; set; }
        public string Tipe { get; set; }
        public string Nik { get; set; }
        public string fstatus { get; set; } = "0";
        public string Keterangan { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedIn { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedIn { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public string StEdit { get; set; } = "0";
        public string DeleteBy { get; set; }
        public DateTime? DeleteTime { get; set; }
        public string pycostcode { get; set; }
        public byte? fprint { get; set; }

        public List<MedicalOutPatientDetailModel> Details { get; set; }
    }


    public class MedicalOutPatientDetailModel
    {
        public string NoReg { get; set; }
        public Guid PersonGuid { get; set; }
        public DateTime TglDet { get; set; }
        public string Person { get; set; }
        public decimal HrgClaim { get; set; }
        public float PersenClaim { get; set; }
        public decimal BiayaObat { get; set; }
        public decimal BiayaKons { get; set; }
        public decimal BiayaLab { get; set; }
        public decimal BiayaLain { get; set; }
        public decimal BiayaTot { get; set; }
        public byte? tDokter { get; set; }
        public string JDokter { get; set; }
        public string kdSpesialis { get; set; }
        public string kdDiagnosa { get; set; }
        public string dremarks { get; set; }
        public string dokter_remark { get; set; }
    }
}
