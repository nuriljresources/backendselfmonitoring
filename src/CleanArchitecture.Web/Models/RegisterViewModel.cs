﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;


namespace CleanArchitecture.Web.Models
{
    public class RegisterViewModel
    {
        [Required]
        [StringLength(60, MinimumLength = 2)]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 5)]
        public string Password { get; set; }

        //public IFormFile ImageData { get; set; }
    }
}
