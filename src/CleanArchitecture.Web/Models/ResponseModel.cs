﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArchitecture.Web.Models
{
    public class ResponseModel
    {
        public ResponseModel()  // static ctor
        {
            IsSuccess = true;
            MessageList = new List<string>();
            Message = "";
        }

        public ResponseModel(bool isSuccess)  // static ctor
        {
            MessageList = new List<string>();
            IsSuccess = isSuccess;
            Message = "";
        }

        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public Object ResponseObject { get; set; }
        public List<string> MessageList { get; set; }
        public bool IsTimeOut { get; set; }

        public void SetError()
        {
            this.IsSuccess = false;
        }

        public void SetError(string message)
        {
            this.IsSuccess = false;
            this.Message = message;
        }

        public void SetError(string text, params object[] args)
        {
            this.IsSuccess = false;
            this.Message = string.Format(text, args);
        }

        public void SetSuccess()
        {
            SetSuccess(string.Empty);
        }

        public void SetSuccess(string message)
        {
            this.IsSuccess = true;
            this.Message = message;
        }

        public void SetSuccess(string text, params object[] args)
        {
            this.IsSuccess = true;
            this.Message = string.Format(text, args);
        }

        public void AddMessageList(string message)
        {
            MessageList.Add(message);
        }

        public void MessageToString()
        {
            if (MessageList != null && MessageList.Count > 0)
            {
                this.Message = string.Join(Environment.NewLine, MessageList.ToArray());
            }
        }
    }
}
