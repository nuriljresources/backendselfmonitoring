using System.Collections.Generic;

public class PagingParameterModel
{
    public string NIKSite { get; set; }
    public string SearchValue { get; set; }

    public int TotalPages { get; set; }
    public int TotalCount { get; set; }
    private int _pageNumber = 1;
    public int PageNumber
    {
        get => _pageNumber;
        set
        {
            if ((value > 0) )
            {
                _pageNumber = value;
            }
        }
    }


    public bool IsNext { get; set; } = true;
}

public class param14Quarantine
{
    public string NIKSite { get; set; }
    public string DateTimeStart { get; set; }
}