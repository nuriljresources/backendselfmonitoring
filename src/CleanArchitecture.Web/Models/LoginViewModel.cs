﻿using System.ComponentModel.DataAnnotations;

namespace CleanArchitecture.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }


        /// <summary>
        /// For User without Active Directory
        /// </summary>
        public bool IsHRMS { get; set; }

        public string FullName { get; set; }
        public string CompanyName { get; set; }
    }
}
