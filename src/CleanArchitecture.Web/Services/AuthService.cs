﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using CryptoHelper;
using System.Collections.Generic;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.Web.Models;
using Microsoft.Extensions.Configuration;
using CleanArchitecture.Infrastructure.Data;

namespace CleanArchitecture.Web.Services
{
    public class AuthService : IAuthService
    {
        string jwtSecret;
        int jwtLifespan;
        public AuthService(IConfiguration config)
        {
            jwtSecret = config.GetValue<string>("JWTSecretKey");
            jwtLifespan = config.GetValue<int>("JWTLifespan");
        }
        public AuthDataModel GetAuthData(UserLogin _user)
        {
            var expirationTime = DateTime.UtcNow.AddSeconds(jwtLifespan);

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(jwtSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("UserId", _user.Id.ToString()),
                    new Claim(ClaimTypes.Name, _user.FullName.ToString()),
                    new Claim(ClaimTypes.Email , _user.Email.ToString()),
                    new Claim(ClaimTypes.Role , _user.Role.ToString())
                }),
                Expires = expirationTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            string token = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));




            return new AuthDataModel
            {
                Id = _user.Id,
                Name = _user.FullName,
                Role = _user.Role,
                Token = token,
                TokenExpirationTime = ((DateTimeOffset)expirationTime).ToUnixTimeSeconds()
            };
        }

        private IEnumerable<Claim> GetUserClaims(UserLogin user)
        {
            IEnumerable<Claim> claims = new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.FullName),
                    new Claim(ClaimTypes.Role, user.Role),
                    new Claim(ClaimTypes.Email, user.Email)
                };
            return claims;
        }
        
        public string HashPassword(string password)
        {
            return Crypto.HashPassword(password);
        }

        public bool VerifyPassword(string actualPassword, string hashedPassword)
        {
            return Crypto.VerifyHashedPassword(hashedPassword, actualPassword);
        }
    }
}
