﻿using CleanArchitecture.Web.CustomAttributes;
using CleanArchitecture.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArchitecture.Web.Services
{
    public class UserService
    {
        public UserMobileModel GetUserByNIKSite(string _nikSite)
        {
            ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);
            string queryEMP = string.Format(
                @"SELECT  
                    Nama,
                    NIKSITE, 
                    ISNULL(email, '') AS email,
                    NmCompany, 
                    nmDepar, 
                    nmJabat, 
                    kdSite,
                    pycostcode
                    FROM V_HRMS_EMP WHERE NIKSite = '{0}' AND active = 1", _nikSite);
            SqlDataReader reader = ConnectionCheck.ExecDataReader(queryEMP, "", "");

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    return new UserMobileModel()
                    {
                        Token = string.Empty,
                        FullName = reader.GetString(0),
                        NIKSite = reader.GetString(1),
                        Email = reader.GetString(2),
                        CompanyName = reader.GetString(3),
                        DepartmentName = reader.GetString(4),
                        Role = reader.GetString(5),
                        SiteCode = reader.GetString(6),
                        CostCode = reader.GetString(7),
                    };
                }
            }

            return null;
        }
    }
}
