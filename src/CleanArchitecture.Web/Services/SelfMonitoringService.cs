﻿using CleanArchitecture.Core.Entities;
using CleanArchitecture.SharedKernel.Interfaces;
using CleanArchitecture.Web.CustomAttributes;
using CleanArchitecture.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArchitecture.Web.Services
{
    public class SelfMonitoringService
    {
        private readonly IRepository _repository;
        SelfMonitoringModel model { get; set; }
        ResponseModel response { get; set; }
        public SelfMonitoringService(IRepository Repository, SelfMonitoringModel _selfMonitoring)
        {
            _repository = Repository;
            model = _selfMonitoring;
            response = new ResponseModel();
        }

        void Validation()
        {
            if (model.IsHRMS)
            {
                if (string.IsNullOrEmpty(model.NIKSite))
                {
                    response.AddMessageList("NIK Site tidak boleh kosong");
                }
                else
                {
                    var _user = new UserMobileModel();

                    ADOHelper ConnectionCheck = new ADOHelper(AppSettingHelper.HRMSConnectionString);

                    string queryEMP = string.Format(
                        @"SELECT TOP 1 NIKSITE, Nama, NmCompany, nmDepar, nmJabat, kdSite FROM V_HRMS_EMP WHERE NIKSite = '{0}' AND active = 1
                        UNION ALL
                        SELECT NIK, Nama,
                        ISNULL((select nmVenLS from V_HRMS_LSPOS where V_HRMS_LSEMP.vendor = V_HRMS_LSPOS.vendor), '') AS NmCompany,
                        '' AS nmDepar,
                        ISNULL((select nmjabat from V_HRMS_LSVENDOR where Kdjabat = V_HRMS_LSEMP.kdjabatan), '') AS nmJabat, 
                        kdSite  
                        FROM V_HRMS_LSEMP WHERE NIK = '{0}' AND active = 1", model.NIKSite);

                    SqlDataReader reader = ConnectionCheck.ExecDataReader(queryEMP
                        , "Year", model.NIKSite);

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            model.NIKSite = reader.GetString(0);
                            model.FullName = reader.GetString(1);
                            model.CompanyName = reader.GetString(2);
                        }
                    }
                    else
                    {
                        response.AddMessageList("NIK tidak ditemukan");
                    }

                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.NIKCompany))
                {
                    model.NIKCompany = model.NIKSite;
                }

                if (string.IsNullOrEmpty(model.FullName))
                {
                    response.AddMessageList("Nama lengkap tidak boleh kosong");
                }

                if (string.IsNullOrEmpty(model.NIKCompany))
                {
                    response.AddMessageList("Nomor induk karyawan tidak boleh kosong");
                }

                if (string.IsNullOrEmpty(model.CompanyName))
                {
                    response.AddMessageList("Nama perusahaan tidak boleh kosong");
                }
            }

            if (string.IsNullOrEmpty(model.SiteCode))
            {
                response.AddMessageList("Lokasi site harus dipilih");
            }

            if (string.IsNullOrEmpty(model.DateTimeCheckInStr))
            {
                response.AddMessageList("Waktu penginputan tidak boleh kosong");
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                var DateTimeCheckIn = DateTime.Parse(model.DateTimeCheckInStr);
                //if (model.IsFromMobile)
                //{
                //    if (DateTime.Now.ToString("tt") == "PM" && DateTime.Now.Hour > 13 && DateTimeCheckIn.Hour < 13)
                //    {
                //        DateTimeCheckIn = DateTime.Parse(model.DateTimeCheckInStr + " PM");
                //    }
                //}

                if (DateTimeCheckIn > dtNow.AddHours(1))
                {
                    response.AddMessageList("Waktu input tidak boleh lebih dari hari ini " + dtNow.AddHours(1).ToString("HH:mm tt") + " (time server)");
                }

                if (DateTimeCheckIn < dtNow.AddHours(-1))
                {
                    response.AddMessageList("Waktu input tidak boleh lebih dari hari ini " + dtNow.AddHours(-1).ToString("HH:mm tt") + " (time server)");
                }
            }

            if (string.IsNullOrEmpty(model.BodyTemperatureStr))
            {
                response.AddMessageList("Suhu tubuh tidak boleh kosong");
            } else
            {
                var BodyTemperature = Decimal.Parse(model.BodyTemperatureStr);
                if(BodyTemperature > 41)
                {
                    response.AddMessageList("Suhu tubuh tidak boleh melebihi batas normal 41℃");
                }
            }

            if (response.MessageList != null && response.MessageList.Count > 0)
            {
                response.MessageToString();
                response.SetError();
            }

            //if (model.IsFromMobile)
            //{
            //    if (string.IsNullOrEmpty(model.Address) || (!string.IsNullOrEmpty(model.Address) && model.Address == "not load"))
            //    {
            //        response.AddMessageList("Address Cannot be empty");
            //    }

            //}

            if (response.MessageList != null && response.MessageList.Count > 0)
            {
                response.MessageToString();
                response.SetError();
            }
        }
        public ResponseModel Save()
        {
            this.Validation();

            if (response.IsSuccess)
            {
                string photoName = string.Empty;
                #region UPLOAD PHOTO
                if (model.Image != null && model.Image.Length > 0)
                {
                    string extension = Path.GetExtension(model.Image.FileName);
                    photoName = string.Format("{0}-{1}-{2}{3}", model.NIKSite, DateTime.Now.ToString("yyyyMMdd-HHmm"), Guid.NewGuid().ToString().Split("-")[0].ToUpper(), extension);

                    var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot", "files", photoName
                        );

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        model.Image.CopyTo(stream);
                    }
                }
                #endregion

                if (model.IsHRMS)
                {
                    ADOHelper ConnectionCheck = new ADOHelper("data source=20.110.1.5;initial catalog=JRN_TEST;user id=dev;password=hrishris;");

                    string queryEMP = string.Format(
                        @"SELECT TOP 1 NIKSITE, Nama, NmCompany, nmDepar, nmJabat, kdSite FROM V_HRMS_EMP WHERE NIKSite = '{0}' AND active = 1
                        UNION ALL
                        SELECT NIK, Nama,
                        ISNULL((select nmVenLS from V_HRMS_LSPOS where V_HRMS_LSEMP.vendor = V_HRMS_LSPOS.vendor), '') AS NmCompany,
                        '' AS nmDepar,
                        ISNULL((select nmjabat from V_HRMS_LSVENDOR where Kdjabat = V_HRMS_LSEMP.kdjabatan), '') AS nmJabat, 
                        kdSite  
                        FROM V_HRMS_LSEMP WHERE NIK = '{0}' AND active = 1"
                    , model.NIKSite);
                    SqlDataReader reader = ConnectionCheck.ExecDataReader(queryEMP
                        , "Year", model.NIKSite);

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            model.NIKSite = reader.GetString(0);
                            model.FullName = reader.GetString(1);
                            model.CompanyName = reader.GetString(2);
                        }
                    }
                } else
                {
                    if(!string.IsNullOrEmpty(model.NIKCompany))
                    {
                        model.NIKSite = model.NIKCompany;
                    }
                }

                model.DateTimeCheckIn = DateTime.Parse(model.DateTimeCheckInStr);
                model.BodyTemperature = Decimal.Parse(model.BodyTemperatureStr);
                
                //if(model.IsFromMobile)
                //{
                //    if(DateTime.Now.ToString("tt") == "PM" && DateTime.Now.Hour > 13 && model.DateTimeCheckIn.Hour < 13)
                //    {
                //        model.DateTimeCheckIn = DateTime.Parse(model.DateTimeCheckInStr + " PM");
                //    }
                //}

                bool IsNight = false;
                if (model.DateTimeCheckIn.Hour >= 17)
                {
                    IsNight = true;
                }

                var _selfMonitoring = _repository.GetSingle<SelfMonitoring>(x => x.NIKSite == model.NIKSite && x.DateCheckIn == model.DateTimeCheckIn.Date);
                if (_selfMonitoring != null)
                {
                    #region UPDATE SELFMONITORING
                    if (IsNight)
                    {
                        _selfMonitoring.BodyTemperatureNight = model.BodyTemperature;
                        _selfMonitoring.DateTimeNight = model.DateTimeCheckIn;
                        _selfMonitoring.PhotoNight = photoName;
                    }
                    else
                    {
                        _selfMonitoring.BodyTemperature = model.BodyTemperature;
                        _selfMonitoring.DateTimeMorning = model.DateTimeCheckIn;
                        _selfMonitoring.PhotoMorning = photoName;
                    }

                    if (string.IsNullOrEmpty(_selfMonitoring.Address) && !string.IsNullOrEmpty(model.Address))
                    {
                        _selfMonitoring.Address = model.Address;
                    }

                    if ((!_selfMonitoring.Latitude.HasValue) && model.Latitude.HasValue)
                    {
                        _selfMonitoring.Latitude = model.Latitude;
                    }

                    if ((!_selfMonitoring.Longitude.HasValue) && model.Longitude.HasValue)
                    {
                        _selfMonitoring.Longitude = model.Longitude;
                    }

                    _selfMonitoring.SiteCode = model.SiteCode;
                    _selfMonitoring.SiteName = model.SiteName;
                    _selfMonitoring.IsOnDuty = model.IsOnDuty;
                    _selfMonitoring.IsSoreThroat = model.IsSoreThroat;
                    _selfMonitoring.IsFlu = model.IsFlu;
                    _selfMonitoring.IsFever = model.IsFever;
                    _selfMonitoring.IsCough = model.IsCough;
                    _selfMonitoring.IsShortness = model.IsShortness;
                    _selfMonitoring.IsContactPatient = model.IsContactPatient;
                    _selfMonitoring.IsQuarantine = model.IsQuarantine;

                    _selfMonitoring.IsOnDuty = model.IsOnDuty;
                    _selfMonitoring.IsFromMobile = model.IsFromMobile;
                    _selfMonitoring.UpdatedDate = DateTime.Now;

                    _repository.Update<SelfMonitoring>(_selfMonitoring);
                    response.SetSuccess(
                        string.Format("{0} berhasil memperbarui data \n pada {1}", _selfMonitoring.FullName, (IsNight ? "malam" : "pagi"))
                        );
                    #endregion
                }
                else
                {
                    #region INSERT SELFMONITORING
                    _selfMonitoring = new SelfMonitoring()
                    {
                        Id = Guid.NewGuid(),
                        DateCheckIn = model.DateTimeCheckIn,
                        NIKSite = model.NIKSite,
                        IsEmployee = model.IsHRMS,
                        SiteCode = (!string.IsNullOrEmpty(model.SiteCode) ? model.SiteCode : string.Empty),
                        SiteName = (!string.IsNullOrEmpty(model.SiteName) ? model.SiteName : string.Empty),
                        CompanyName = model.CompanyName,
                        FullName = model.FullName,
                        Address = (!string.IsNullOrEmpty(model.Address) ? model.Address : string.Empty),
                        Latitude = model.Latitude,
                        Longitude = model.Longitude,
                        IsSoreThroat = model.IsSoreThroat,
                        IsFlu = model.IsFlu,
                        IsFever = model.IsFever,
                        IsCough = model.IsCough,
                        IsShortness = model.IsShortness,
                        IsContactPatient = model.IsContactPatient,
                        IsQuarantine = model.IsQuarantine,

                        IsFromMobile = model.IsFromMobile,
                        IsOnDuty = model.IsOnDuty,
                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now,
                    };

                    if (IsNight)
                    {
                        _selfMonitoring.BodyTemperatureNight = model.BodyTemperature;
                        _selfMonitoring.DateTimeNight = model.DateTimeCheckIn;
                        _selfMonitoring.PhotoNight = photoName;
                        _selfMonitoring.PhotoMorning = string.Empty;
                    }
                    else
                    {
                        _selfMonitoring.BodyTemperature = model.BodyTemperature;
                        _selfMonitoring.DateTimeMorning = model.DateTimeCheckIn;
                        _selfMonitoring.PhotoMorning = photoName;
                        _selfMonitoring.PhotoNight = string.Empty;
                    }

                    _repository.Add(_selfMonitoring);
                    _repository.Commit();
                    response.SetSuccess(
                        string.Format("{0} berhasil menyimpan\npada {1}", _selfMonitoring.FullName, (IsNight ? "malam" : "pagi"))
                        );
                    #endregion
                }
            }

            return response;
        }
    
        public IQueryable<SelfMonitoring> GetList(string NIKSite)
        {
            return _repository.GetAll<SelfMonitoring>()
                .Where(x => x.NIKSite == NIKSite).AsQueryable();
        }


        public List<SelfMonitoringViewModel> Get14DaysHistory(string NIKSite, DateTime _dateStart)
        {
            DateTime dateStart = DateTime.Now;
            if(_dateStart != null)
            {
                dateStart = _dateStart;
            }

            dateStart = dateStart.Date.AddDays(1).AddMinutes(-1);

            int PageSize = AppSettingHelper.PageSize;

            var source = _repository.GetAll<SelfMonitoring>()
                .Where(x => x.NIKSite == NIKSite && x.DateCheckIn <= _dateStart)
                .OrderByDescending(x => x.DateCheckIn)
                .Take(14)
                .Select(x => new SelfMonitoringViewModel()
                {
                    DateTimeCheckIn = x.DateCheckIn,
                    NIKSite = x.NIKSite,
                    FullName = x.FullName,
                    Photo = (!string.IsNullOrEmpty(x.PhotoNight) ? x.PhotoNight : x.PhotoMorning),
                    SiteName = x.SiteName,
                    CompanyName = x.CompanyName,
                    BodyTemperatureMorning = (x.BodyTemperature.HasValue ? x.BodyTemperature.Value : 0),
                    BodyTemperatureNight = (x.BodyTemperatureNight.HasValue ? x.BodyTemperatureNight.Value : 0),
                    IsSoreThroat = x.IsSoreThroat,
                    IsFlu = x.IsFlu,
                    IsFever = x.IsFever,
                    IsCough = x.IsCough,
                    IsShortness = x.IsShortness,
                    IsOnDuty = x.IsOnDuty,
                    IsContactPatient = x.IsContactPatient,
                    IsQuarantine = x.IsQuarantine,
                    IsCheckIn = true
                }).ToList();


            List<SelfMonitoringViewModel> _list = new List<SelfMonitoringViewModel>();
            for (int i = 0; i < 14; i++)
            {
                DateTime dtCheckIn = dateStart.AddDays(-i).Date;
                var _item = source.FirstOrDefault(x => x.DateTimeCheckIn.Date == dtCheckIn);
                if(_item == null)
                {
                    _item = new SelfMonitoringViewModel();
                    _item.DateTimeCheckIn = dtCheckIn;
                    _item.IsCheckIn = false;
                }

                _list.Add(_item);
            }

            return _list;
        }
    }
}
