﻿using CleanArchitecture.Core.Entities;
using CleanArchitecture.Web.ApiModels;
using CleanArchitecture.Web.Models;
using System;

namespace CleanArchitecture.Web.Services.Abstraction
{
    public interface IAuthService
    {
        string HashPassword(string password);
        bool VerifyPassword(string actualPassword, string hashedPassword);
        AuthDataModel GetAuthData(UserLogin _user);
    }
}
