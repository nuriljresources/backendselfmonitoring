﻿using CleanArchitecture.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CleanArchitecture.SharedKernel.Interfaces
{
    public interface IRepository
    {
        T GetById<T>(Guid id) where T : BaseEntity;
        List<T> List<T>() where T : BaseEntity;
        T Add<T>(T entity) where T : BaseEntity;
        void Update<T>(T entity) where T : BaseEntity;
        void Delete<T>(T entity) where T : BaseEntity;

        IEnumerable<T> AllIncluding<T>(params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        IEnumerable<T> GetAll<T>() where T : BaseEntity;
        int Count<T>() where T : BaseEntity;
        T GetSingle<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity;
        T GetSingle<T>(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        IEnumerable<T> FindBy<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity;


        void DeleteWhere<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity;
        void Commit();
    }
}