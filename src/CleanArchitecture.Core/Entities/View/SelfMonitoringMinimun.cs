﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArchitecture.Core.Entities
{
    public class SelfMonitoringMinimun
    {
        public string NIKSite { get; set; }
        public string FullName { get; set; }
        public DateTime DateCheckIn { get; set; }
    }
}