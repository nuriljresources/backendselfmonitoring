﻿using CleanArchitecture.Core.Events;
using CleanArchitecture.Core.SharedKernel;

namespace CleanArchitecture.Core.Entities
{
    public class SiteLocation
    {
        public string kdCompany { get; set; }
        public string nmCompany { get; set; }
        public string kdSite { get; set; }
        public string nmSite { get; set; }
    }
}
