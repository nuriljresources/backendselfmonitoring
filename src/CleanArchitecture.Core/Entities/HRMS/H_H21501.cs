﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArchitecture.Core.Entities.HRMS
{
    public class H_H21501
    {
        public string NoReg { get; set; }
        public Guid PersonGuid { get; set; }
        public DateTime TglDet { get; set; }
        public string Person { get; set; }
        public decimal? HrgClaim { get; set; }
        public float? PersenClaim { get; set; }
        public decimal? BiayaObat { get; set; }
        public decimal? BiayaKons { get; set; }
        public decimal? BiayaLab { get; set; }
        public decimal? BiayaLain { get; set; }
        public decimal? BiayaTot { get; set; }
        public byte? tDokter { get; set; }
        public string JDokter { get; set; }
        public string kdSpesialis { get; set; }
        public string kdDiagnosa { get; set; }
        public string dremarks { get; set; }
        public string dokter_remark { get; set; }
    }
}
