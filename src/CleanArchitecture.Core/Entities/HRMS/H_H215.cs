﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CleanArchitecture.Core.Entities.HRMS
{
    public class H_H215
    {
        [Key]
        public string NoReg { get; set; }
        public string NoRef { get; set; }
        public string KdSite { get; set; }
        public DateTime? TglTrans { get; set; }
        public string Tipe { get; set; }
        public string Nik { get; set; }
        public string fstatus { get; set; }
        public string Keterangan { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedIn { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedIn { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public string StEdit { get; set; }
        public string DeleteBy { get; set; }
        public DateTime? DeleteTime { get; set; }
        public string pycostcode { get; set; }
        public byte? fprint { get; set; }
    }
}
