﻿using CleanArchitecture.Core.SharedKernel;
using System;

namespace CleanArchitecture.Core.Entities
{
    public class SelfMonitoring : BaseEntity
    {
        public string NIKSite { get; set; }
        public DateTime DateCheckIn { get; set; }
        public bool IsEmployee { get; set; }
        public DateTime? DateTimeMorning { get; set; }
        public DateTime? DateTimeNight { get; set; }
        public string CompanyName { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? BodyTemperature { get; set; }
        public decimal? BodyTemperatureNight { get; set; }
        public bool IsSoreThroat { get; set; }
        public bool IsFlu { get; set; }
        public bool IsFever { get; set; }
        public bool IsCough { get; set; }
        public bool IsShortness { get; set; }
        public bool IsContactPatient { get; set; }
        public bool IsQuarantine { get; set; }


        public string PhotoMorning { get; set; }
        public string PhotoNight { get; set; }

        public bool IsFromMobile { get; set; }
        public bool IsOnDuty { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
