﻿using CleanArchitecture.Core.SharedKernel;
using System;

namespace CleanArchitecture.Core.Entities
{
    public class SelfMonitoringLog : BaseEntity
    {
        public Guid SelfMonitoringID { get; set; }
        public DateTime DateCheckIn { get; set; }
        public decimal BodyTemperature { get; set; }
        public string Condition { get; set; }
    }
}
