//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("CleanArchitecture.Infrastructure", "C:\\Users\\NURIL\\source\\repos3\\CleanArchitecture\\src\\CleanArchitecture.Infrastructu" +
    "re", "CleanArchitecture.Infrastructure.csproj", "0")]
[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("CleanArchitecture.UnitTests", "C:\\Users\\NURIL\\source\\repos3\\CleanArchitecture\\tests\\CleanArchitecture.Tests", "CleanArchitecture.UnitTests.csproj", "0")]
[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("CleanArchitecture.Web", "C:\\Users\\NURIL\\source\\repos3\\CleanArchitecture\\src\\CleanArchitecture.Web", "CleanArchitecture.Web.csproj", "0")]
[assembly: System.Reflection.AssemblyCompanyAttribute("Clean Architecture")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Debug")]
[assembly: System.Reflection.AssemblyCopyrightAttribute("Copyright © Clean Architecture 2020")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("1.0.0.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("1.0.0")]
[assembly: System.Reflection.AssemblyProductAttribute("Clean Architecture Projects")]
[assembly: System.Reflection.AssemblyTitleAttribute("CleanArchitecture.IntegrationTests")]
[assembly: System.Reflection.AssemblyVersionAttribute("1.0.0.0")]

// Generated by the MSBuild WriteCodeFragment class.

